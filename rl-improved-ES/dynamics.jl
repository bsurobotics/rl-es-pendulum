mutable struct dynamics
    θ           ::Float32
    θdot        ::Float32
    x           ::Array{Float32, 1}
    a           ::Float32
    ϵ           ::Float32
    τ           ::Float32
    function dynamics()
        θ       = 0.0f0
        θdot    = 0.0f0
        x       = vcat(cos(θ), sin(θ), θdot)
        a       = 1.0f0
        ϵ       = 0.0f0
        τ       = 0.0f0
        new(θ, θdot, x, a, ϵ, τ)
    end
end

cartesian(s)        = [cos.(s[1,:]) sin.(s[1,:]) s[2,:]]'
# cartesian(s)        = [cos(s[1]), sin(s[1]), s[2]]
cylinderical(x)     = [rem2pi(atan(x[2],x[1]), RoundNearest), x[3]]

function set_state(pen::dynamics, x)
    pen.θ, pen.θdot = cylinderical(x)
    pen.x           = x 
end

function set_control(pen::dynamics, τ)
    pen.τ = τ
end

function expert_control(pen::dynamics, s)
    H̃       = 0.5f0*s[2]^2.0f0 + pen.a*(1.0f0+cos(s[1])) - 2.0f0*pen.a
    pen.τ   = clamp(-k*s[2]*H̃, -sat*pen.a, sat*pen.a)
end

function eom(dx, x, p, t)
    dx[1] = -x[2]*x[3] - 1.0f0/10.0f0*x[1]*(x[1]^2.0f0 + x[2]^2.0f0 - 1)
    dx[2] = x[1]*x[3] - 1.0f0/10.0f0*x[2]*(x[1]^2.0f0 + x[2]^2.0f0 - 1)
    dx[3] = pen.τ + pen.a*x[2]
end

function integrate(pen::dynamics, xi, t)
    prob        = ODEProblem(eom, xi, (t,t + 0.05))
    integrator  = init(prob, Tsit5())
    step!(integrator)

    return integrator.u, integrator.t
end

function test_dynamics(pen::dynamics)
    si              = [2.0f0, 0.0f0]
    xi              = cartesian(si)
    t               = 0.0f0
    states          = Array{Array{Float32, 1}, 1}()
    T               = Float32[]
    τ               = Float32[]
    θi              = atan(xi[2], xi[1])

    for i in 1:500
        expert_control(pen, si)
        xi, t       = integrate(pen, xi, t)
        θ, θdot     = (atan(xi[2],xi[1]), xi[3])

        if abs(θ-θi)> pi     #this handles wrapping when converting to polar coordinates
            θ_op    = [θ-2.0f0*pi, θ+2.0f0*pi]
            θ       = θ_op[argmin(abs.([θ - 2.0f0*pi - θi, θ + 2.0f0*pi - θi]))]
            θi      = θ
        else
            θi      = atan(xi[2], xi[1])
        end
        si = [θ, θdot]
        push!(states, [θ, θdot])
        push!(T, t)
        push!(τ, pen.τ)
    end
    fig.axes[1].plot(getindex.(states, 1), getindex.(states, 2))
    return states, T, τ
end

function test_dynamics(pen::dynamics, s)
    si              = s
    xi              = cartesian(si)
    t               = 0.0f0
    states          = Array{Array{Float32, 1}, 1}()
    T               = Float32[]
    τ               = Float32[]
    θi              = atan(xi[2], xi[1])

    for i in 1:500
        expert_control(pen, si)
        xi, t       = integrate(pen, xi, t)
        θ, θdot     = (atan(xi[2],xi[1]), xi[3])

        if abs(θ-θi)> pi     #this handles wrapping when converting to polar coordinates
            θ_op    = [θ-2.0f0*pi, θ+2.0f0*pi]
            θ       = θ_op[argmin(abs.([θ - 2.0f0*pi - θi, θ + 2.0f0*pi - θi]))]
            θi      = θ
        else
            θi      = atan(xi[2], xi[1])
        end
        si = [θ, θdot]
        push!(states, [θ, θdot])
        push!(T, t)
        push!(τ, pen.τ)
    end
    fig.axes[1].plot(getindex.(states, 1), getindex.(states, 2))
    return states, T, τ
end