using PyPlot
using OrdinaryDiffEq
using Flux
using Printf
using LinearAlgebra
using BSON: @save, @load
using PyCall
using ForwardDiff
using Statistics

include("dynamics.jl")
include("imitation.jl")
include("rl.jl")

# fig = figure(1)
# fig.add_subplot(2, 2, 1), fig.add_subplot(2, 2, 2), fig.add_subplot(2, 2, 3), fig.add_subplot(2, 2, 4)

fig = figure(1)
fig.add_subplot(2, 2, (1,2)), fig.add_subplot(2, 2, 3), fig.add_subplot(2, 2, 4)


pen = dynamics()
imi = imitation()
RL = reinforcementLearning()

k = 0.50f0
sat = 0.5f0
# X, T, τ = test_dynamics(pen)
# train_imitation()

fig.savefig("RL_improvement.eps", bbox_inches="tight", pad_inches=0.1, format="eps")