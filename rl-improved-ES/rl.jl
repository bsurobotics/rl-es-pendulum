
mutable struct reinforcementLearning
    V                       ::Flux.Chain
    V_target                ::Flux.Chain
    batch_size              ::Int
    S                       ::Array{Float32, 2}
    y                       ::Array{Float32, 1}
    D                       ::Dict{Symbol,Array{T,1} where T}
    minibatch               ::Dict{Symbol,Array{T,1} where T}
    episode_length          ::Int
    episodic_rewards        ::Array{Float32, 1}
    avg_rewards             ::Array{Float32, 1}
    γ                       ::Float32
    counter                 ::Int
    s_goal                  ::Array{Float32, 1}
    τ                       ::Float32
    ϵ                       ::Float32
    opt                     ::ADAM

    function reinforcementLearning()
        V                   = Chain(cartesian, Dense(3, 128, relu, initW=Flux.glorot_uniform),
                                Dense(128, 32, relu, initW=Flux.glorot_uniform),
                                Dense(32, 1, initW=Flux.glorot_uniform))
        V_target            = Chain(cartesian, Dense(3, 128, relu, initW=Flux.glorot_uniform),
                                Dense(128, 32, relu, initW=Flux.glorot_uniform),
                                Dense(32, 1, initW=Flux.glorot_uniform))
        
        batch_size          = 64
        S                   = zeros(2, batch_size)
        y                   = zeros(batch_size)

        D                   = Dict(:s => Array{Array{Array{Float32, 1}, 1}, 1}(), 
                                   :a => Array{Float32, 1}(), 
                                   :r => Array{Float32, 1}())

        minibatch           = Dict(:s => Array{Array{Array{Float32, 1}, 1}, 1}(),
                                   :a => Array{Float32, 1}(), 
                                   :r => Array{Float32, 1}())

        episode_length      = 200
        episodic_rewards    = Array{Float32, 1}()
        avg_rewards         =  Array{Float32, 1}()
        counter             = 0
        γ                   = 0.999f0
        s_goal              = [0.0f0, 0.0f0]
        τ                   = 0.0005f0
        ϵ                   = 0.5f0
        opt                 = ADAM()
        new(V, V_target, batch_size, S, y, D, minibatch, episode_length, episodic_rewards, avg_rewards, γ, counter, s_goal, τ, ϵ, opt)
    end
end

rangeCheck(x0, x1, x2) = all(x0 .>= x1) && all(x0 .<= x2) ? true : false

isterminal(s) = rangeCheck(s - RL.s_goal, -0.01f0, 0.01f0)

function cost(RL::reinforcementLearning, s, u)
    costs     = zeros(2)
    costs[1]  = 1.0f0 - cos(s[1])      #norm is differentiable, abs is not
    costs[2]  = 0.5f0*dot(s[2] - RL.s_goal[2], s[2] - RL.s_goal[2])

    return sum(costs)
end

function add_to_replay_buffer(RL::reinforcementLearning, s, s′, a, r)
    push!(RL.D[:s], [[s];[s′]])
    push!(RL.D[:a], a)
    push!(RL.D[:r], r)
end

function sample_minibatch(RL::reinforcementLearning)
    map(empty!, (RL.minibatch[:s], RL.minibatch[:a], RL.minibatch[:r]))

    for i in 1:RL.batch_size
        pick = rand(1:length(RL.D[:s]))
        push!(RL.minibatch[:s], RL.D[:s][pick])
        push!(RL.minibatch[:a], RL.D[:a][pick])
        push!(RL.minibatch[:r], RL.D[:r][pick])
    end
end

clear_buffer(RL::reinforcementLearning) = (length(RL.D[:s]) > 10000) ? map(popfirst!, (RL.D[:s], RL.D[:a], RL.D[:r])) : nothing

value_loss(x, w) = Flux.mse(RL.V(x), w') + 
                10000.0f0*sum( sum(abs2, vec(RL.V[i].W - imi.V[i].W)) for i = 2:length(RL.V) ) +
                10000.0f0*sum( sum(abs2, vec(RL.V[i].b - imi.V[i].b)) for i = 2:length(RL.V) )
                # 1000.0f0/(1+sqrt(RL.counter))*sum( Flux.mse(RL.V[i].W, imi.V[i].W) for i = 2:length(RL.V)) + 
                # 1000.0f0/(1+sqrt(RL.counter))*sum( Flux.mse(RL.V[i].b, imi.V[i].b) for i = 2:length(RL.V))

function update_labels(RL::reinforcementLearning)
    RL.S    = zeros(2, RL.batch_size)
    RL.y    = zeros(RL.batch_size)
    
    for i in 1:RL.batch_size
        s           = RL.minibatch[:s][i][1]
        s′          = RL.minibatch[:s][i][2]
        RL.S[:,i]   = s
        RL.y[i]     = RL.minibatch[:r][i] + RL.γ * RL.V_target(s′)[1]     #check if the behavior of H̃ is similar to value function
    end
end

function update_V_parameters(RL::reinforcementLearning)
    dataset = Base.Iterators.repeated((RL.S, RL.y), 1)
    Flux.train!(value_loss, Flux.params(RL.V), dataset, RL.opt)
end

function update_V_target_parameters(RL::reinforcementLearning)

    if mod(RL.counter, 2) == 0
        for i in 2:length(RL.V)
            RL.V_target[i].W[:,:] = RL.τ * RL.V[i].W + (1.0 - RL.τ)*RL.V_target[i].W
            RL.V_target[i].b[:,:] = RL.τ * RL.V[i].b + (1.0 - RL.τ)*RL.V_target[i].b
        end
    end
end

function RL_controller(RL::reinforcementLearning, s)
    g       = s -> ForwardDiff.jacobian(RL.V, s)
    ∂V      = g(s)[2]
    pen.τ   = clamp(-k*∂V, -sat*pen.a, sat*pen.a)
end

function generate_episode(RL::reinforcementLearning, si)
    RL_controller(RL, si)
    xi          = cartesian(si)
    x, t        = integrate(pen, xi, 0.0f0)
    s           = cylinderical(x)
    r           = cost(RL, s, pen.τ)
    return s, pen.τ, r 
end

function load_imitated_weights(filename)
    @load filename V
    RL.V = deepcopy(V)
    RL.V_target = deepcopy(V)
    imi.V = deepcopy(V)
end

function train_RL(RL::reinforcementLearning, train_iter)
    # load_imitated_weights("V_imi.bson")
    for i in 1:train_iter
        si = random_state()
        R = 0.0f0

        for i in 1:RL.episode_length
            length(RL.D[:s]) < RL.batch_size ? iter = RL.batch_size : iter = 1

            for i in 1:iter
                s′, a, r = generate_episode(RL, si)     #take one step
                add_to_replay_buffer(RL, si, s′, a, r)
                si = s′
                R += r
            end

            sample_minibatch(RL)
            update_labels(RL)
            update_V_parameters(RL)
            update_V_target_parameters(RL)
            RL.counter +=1
            
            if mod(RL.counter, 200) == 0 
                @printf "%i%%  \n" RL.counter/(train_iter*RL.episode_length) *100.0f0
                # @printf " Error = %f \n" test_error()
            end
        end

        push!(RL.episodic_rewards, R) 
        plot_episodic_costs()

        # if mod(RL.counter, 50) == 0 
        #     fig.axes[1].cla()
        #     test_RL(random_state()) 
        # end
    end
end

function test_RL(s)
    θi              = s[1]
    si              = s
    xi              = cartesian(si)
    t               = 0.0f0
    states          = Array{Array{Float32, 1}, 1}()
    T               = Float32[]
    τ               = Float32[]

    for i in 1:500
        RL_controller(RL, si)
        xi, t       = integrate(pen, xi, t)
        θ, θdot     = (atan(xi[2],xi[1]), xi[3])

        if abs(θ-θi)> pi     #this handles wrapping when converting to polar coordinates
            θ_op    = [θ-2.0f0*pi, θ+2.0f0*pi]
            θ       = θ_op[argmin(abs.([θ - 2.0f0*pi - θi, θ + 2.0f0*pi - θi]))]
            θi      = θ
        else
            θi      = atan(xi[2], xi[1])
        end

        si = [θ, θdot]
        push!(states, [θ, θdot])
        push!(T, t)
        push!(τ, pen.τ)
    end

    fig.axes[1].plot(getindex.(states, 1), getindex.(states, 2))
    return states, T, τ
end

function test_RL(pen::dynamics, s)
    θi              = s[1]
    si              = s
    xi              = cartesian(si)
    t               = 0.0f0
    states          = Array{Array{Float32, 1}, 1}()
    T               = Float32[]
    τ               = Float32[]

    for i in 1:200
        RL_controller(RL, si)
        xi, t       = integrate(pen, xi, t)
        θ, θdot     = (atan(xi[2],xi[1]), xi[3])

        if abs(θ-θi)> pi     #this handles wrapping when converting to polar coordinates
            θ_op    = [θ-2.0f0*pi, θ+2.0f0*pi]
            θ       = θ_op[argmin(abs.([θ - 2.0f0*pi - θi, θ + 2.0f0*pi - θi]))]
            θi      = θ
        else
            θi      = atan(xi[2], xi[1])
        end

        si = [θ, θdot]
        push!(states, [θ, θdot])
        push!(T, t)
        push!(τ, pen.τ)
    end

    fig.axes[1].plot(getindex.(states, 1), getindex.(states, 2), 
        linewidth=2, label=L"u_{RL}(x; p)")
    fig.axes[1].set_title("State evolution", fontsize=16)
    fig.axes[1].set_xlabel(L"$\theta$", fontsize=16)
    fig.axes[1].set_ylabel(L"$\dot{\theta}$", fontsize=16)
    fig.axes[1].tick_params(axis="x", labelsize=16)
    fig.axes[1].tick_params(axis="y", labelsize=16)
    fig.axes[1].legend(fontsize=15)

    return states, T, τ
end


function comparison_RLvsImitated()
    data_size   = 100
    θ           = range(-3.14f0, stop = 3.14f0, length = data_size)
    θdot        = range(-5.0f0, stop = 5.0f0, length = data_size)
    v_RL        = zeros(data_size, data_size)
    v_imitated  = zeros(data_size, data_size)

    for i in 1:data_size
        for j in 1:data_size
            s                   = [θ[i], θdot[j]]
            v_RL[i, j]          = RL.V(s)[1]
            v_imitated[i, j]    = imi.V(s)[1]
        end
    end

    c1          = fig.axes[2].pcolormesh(θ, θdot, v_RL, cmap="RdBu", shading="nearest")
    fig.axes[2].set_title(L"$V_{RL}$", fontsize=16)
    fig.axes[2].set_xlabel(L"$\theta$", fontsize=16)
    fig.axes[2].set_ylabel(L"$\dot{\theta}$", fontsize=16)
    fig.axes[2].tick_params(axis="x", labelsize=16)
    fig.axes[2].tick_params(axis="y", labelsize=16)
    # imi.cb1.remove()
    imi.cb1     = plt.colorbar(c1, ax=fig.axes[2])
    c2          = fig.axes[3].pcolormesh(θ, θdot, v_imitated, cmap="RdBu", shading="nearest")
    fig.axes[3].set_title(L"$V_{original}$", fontsize=16)
    fig.axes[3].set_xlabel(L"$\theta$", fontsize=16)
    fig.axes[3].set_ylabel(L"$\dot{\theta}$", fontsize=16)
    fig.axes[3].tick_params(axis="x", labelsize=16)
    fig.axes[3].tick_params(axis="y", labelsize=16)
    # imi.cb2.remove()
    imi.cb2     = plt.colorbar(c2, ax=fig.axes[3])
end

function plot_episodic_costs()
    if length(RL.episodic_rewards) > 20 
        push!(RL.avg_rewards, mean(RL.episodic_rewards[end-20:end])) 
        fig.axes[2].cla()
        fig.axes[2].plot(RL.avg_rewards)
    end
end