mutable struct imitation
    V       ::Flux.Chain
    β       ::Float32
    D       ::Dict{Symbol,Array{T,1} where T}
    opt     ::ADAM
    S       ::Array{Float32, 2}
    y       ::Array{Float32, 1}
    # fig     ::Figure
    cb1     ::PyCall.PyObject
    cb2     ::PyCall.PyObject

    function imitation()
        # elu(x::Real, α=one(x)) = x > zero(x) ? x : α * (exp(x) - one(1))
        V           = Chain(cartesian, Dense(3, 128, relu, initW=Flux.glorot_uniform),
                            Dense(128, 32, relu, initW=Flux.glorot_uniform),
                            Dense(32, 1, initW=Flux.glorot_uniform))
        β           = 0.3f0
        D           = Dict(:s => Array{Array{Float32, 1}, 1}(), 
                           :v => Array{Float32, 1}())
        opt         = ADAM()
        S           = zeros(2, 1)
        y           = zeros(1)

        data_size   = 100
        θ           = range(-Float32(pi), stop = Float32(pi), length = data_size)
        θdot        = range(-5.0f0, stop = 5.0f0, length = data_size)
        c1          = fig.axes[2].pcolormesh(θ, θdot, zeros(data_size, data_size), cmap="RdBu", shading="nearest")
        c2          = fig.axes[3].pcolormesh(θ, θdot, zeros(data_size, data_size), cmap="RdBu", shading="nearest")
        cb1         = plt.colorbar(c1, ax=fig.axes[2])
        cb2         = plt.colorbar(c2, ax=fig.axes[3])

        new(V, β, D, opt, S, y, cb1, cb2)
    end

end

function V_expert(imi::imitation, s)
    x = cartesian(s)

    H̃ = 0.5f0*x[3]^2.0f0 + pen.a*(1.0f0 + x[1]) - 2.0f0*pen.a
    return 0.5f0*H̃^2.0f0
end

function V_ctrl(s)
    x = cartesian(s)

    H̃ = 0.5f0*x[3,:].^2.0f0 .+ pen.a*(1.0f0 .+ x[1,:]) .- 2.0f0*pen.a
    return s[2,:] .* H̃
end

function imitated_control(pen::dynamics, s)
    # v_exp(x) = 0.5f0*(0.5f0*x[3]^2.0f0 + pen.a*(1.0f0 + x[1]) - 2.0f0*pen.a)^2.0f0
    # g = x -> ForwardDiff.gradient(v_exp, x)
    # ps = Flux.params(s)
    # gs = Flux.gradient( () -> imi.V(s)[1], ps)
    # ∂V = gs[s][2]
    g       = s -> ForwardDiff.jacobian(imi.V, s)
    ∂V      = g(s)[2]
    pen.τ   = clamp(-k*∂V, -sat*pen.a, sat*pen.a)
end

# function imitated_control(pen::dynamics, s)
#     V       = abs(imi.V(s)[1])        #when V = 0, the imitation deviates slightly. Hit with abs incase its a negative deviation
#     # V       = V_expert(imi, x)
#     H̃       = 0.5f0*s[2]^2.0f0 + pen.a*(1.0f0 + cos(s[1])) - 2.0f0*pen.a
#     sgn     = sign(H̃)
#     pen.τ   = clamp(-sgn*k*s[2]*sqrt(2.0f0*V), -1.0f0*pen.a, 1.0f0*pen.a)
# end

function add_data_to_buffer(s, v)
    push!(imi.D[:s], s)
    push!(imi.D[:v], v)
end

function discretize_states()
    data_size   = 1000
    θ           = range(-Float32(pi), stop = Float32(pi), length = data_size)
    θdot        = range(-5.0f0, stop = 5.0f0, length = data_size)
    
    for i in 1:data_size
        for j in 1:data_size
            s       = [θ[i], θdot[j]]
            v       = V_expert(imi, s)
            add_data_to_buffer(s, v)
        end
    end

end

function randomly_generate_data_set(imi::imitation, datasize)
    for i in 1:datasize
        si      = random_state()    
        v       = V_expert(imi, si)
        add_data_to_buffer(si, v)
    end
end

function clear_data_set(imi::imitation, num)
    for i in 1:num
        map(popfirst!, (imi.D[:s], imi.D[:v])) 
    end
end

function create_labels()
    imi.S           = zeros(2, length(imi.D[:s]))
    imi.y           = imi.D[:v]
    
    function construct_training_data(i)
        imi.S[i,:]  = getindex.(imi.D[:s], i)
    end

    map(construct_training_data, range(1, stop = size(imi.S, 1)))
end

imitation_loss(s, label) = Flux.mse(imi.V(s), label') + Flux.mse(∂V∂(imi, s), V_ctrl(s)')

function update_parameters()
    dataset = Base.Iterators.repeated((imi.S, imi.y), 1)
    Flux.train!(imitation_loss, Flux.params(imi.V), dataset, imi.opt)
end

# elu(x::Real, α=one(x)) = x > zero(x) ? x : α * (exp(x) - one(1))
delu(x::Real, α=one(x)) = ifelse(x > 0.0, one(x), α*exp(x) )
drelu(x::Real) = ifelse(x > 0.0, one(x), zero(x) )

function ∂V∂(imi::imitation, s)
    ps = Flux.params(imi.V)
    # W1, b1, W2, b2 = ps     # 1 hidden layer
    # return W2 * ( drelu.(W1*cartesian(s) .+ b1) .* (W1*[0,0,1]) )

    W1, b1, W2, b2, W3, b3 = ps     # 2 hidden layers
    return W3 *(
        drelu.( W2 * relu.( W1 * cartesian(s) + b1 ) + b2 ) .* 
        ( W2 * ( drelu.(W1*cartesian(s) .+ b1) .* (W1*[0,0,1]) ) )
    )
end


function update_imitation_parameters(imi::imitation, repeat)
    ps = Flux.params(imi.V)
    # Flux.mse(imi.V(s), label') + Flux.mse(∂V∂(imi, s), V_ctrl(s)')
    for i in 1:repeat
        # # 1-hidden layer
        # gs = Flux.gradient( () -> (
        #     Flux.mse(imi.V(imi.S), imi.y') + 
        #     1.0f0*Flux.mse(
        #         ps[3] * ( drelu.(ps[1]*cartesian(imi.S) .+ ps[2]) .* (ps[1]*[0,0,1]) ), 
        #         (imi.S[2,:] .* (0.5f0*imi.S[2,:].^2.0f0 .+ pen.a*(1.0f0 .+ cos.(imi.S[1,:])) .- 2.0f0*pen.a))'
        #     )), 
        #     ps)
        # # gs = Flux.gradient( () -> Flux.mse(imi.V(imi.S), imi.y'), ps)

        # 2-hidden layers
        gs = Flux.gradient( () -> (
            Flux.mse(imi.V(imi.S), imi.y') + 
            1.0f0*Flux.mse(
                ps[5] *(
                    drelu.( ps[3] * relu.( ps[1] * cartesian(imi.S) .+ ps[2] ) .+ ps[4] ) .* 
                    ( ps[3] * ( drelu.(ps[1]*cartesian(imi.S) .+ ps[2]) .* (ps[1]*[0,0,1]) ) )
                ) |> vec, 
                (imi.S[2,:] .* (0.5f0*imi.S[2,:].^2.0f0 .+ pen.a*(1.0f0 .+ cos.(imi.S[1,:])) .- 2.0f0*pen.a))'
            )), 
            ps)

        Flux.Optimise.update!(imi.opt, ps, gs) 
    end
end

function random_state()

    pi32 = Float32(pi)
    vmax = 5.0f0
    θ, θdot =  (2.0f0*pi32*rand(Float32) - pi32, 2.0f0*vmax*rand(Float32) - vmax)
    return [θ, θdot]

end

function train_imitation()

    iter = 20000
    counter = 0

    @printf "Imitation training .... \n"

    # discretize_states()

    randomly_generate_data_set(imi, 1000)
    create_labels()

    for i in 1:iter

        # for j in 1:Int(floor(1_000_000/128))
        #     create_labels(j)
        #     update_parameters()
        # end

        # for i in 1:500
        #     randomly_generate_data_set(imi)
        # end
        # create_labels()
        # update_parameters()
        update_imitation_parameters(imi, 1)
        counter += 1

        if mod(counter, 100) == 0 
            @printf "%i%%  |" counter/iter *100.0f0
            @printf " Error = %f \n" test_error()
        end
    end
    # comparison_heat_map()
end


function train_imitation_incrementally()

    iter = 20000
    counter = 0

    @printf "Imitation training .... \n"
    isempty(imi.D[:s]) ? randomly_generate_data_set(imi, 1000) : nothing

    for i in 1:iter

        create_labels()
        update_imitation_parameters(imi, 1)
        clear_data_set(imi, 200)
        randomly_generate_data_set(imi, 200)

        counter += 1

        if mod(counter, 100) == 0 
            @printf "%i%%  |" counter/iter *100.0f0
            @printf " Error = %f \n" test_error()
        end
    end
end

function test_error()
    error = 0.0f0
    data_size = 100

    for i in 1:data_size
        si      = random_state()
        error   += (V_expert(imi, si) - imi.V(si)[1])^2.0f0 + 1.0f0*(∂V∂(imi, si)[1] - V_ctrl(si)[1])^2.0f0
    end

    return error/data_size

end

function test_imitation(pen::dynamics)
    θi              = 2.0f0
    si              = [θi, 0.0f0]
    xi              = cartesian(si)
    t               = 0.0f0
    states          = Array{Array{Float32, 1}, 1}()
    T               = Float32[]
    τ               = Float32[]

    for i in 1:500
        imitated_control(pen, si)
        xi, t       = integrate(pen, xi, t)
        θ, θdot     = (atan(xi[2],xi[1]), xi[3])

        if abs(θ-θi)> pi     #this handles wrapping when converting to polar coordinates
            θ_op    = [θ-2.0f0*pi, θ+2.0f0*pi]
            θ       = θ_op[argmin(abs.([θ - 2.0f0*pi - θi, θ + 2.0f0*pi - θi]))]
            θi      = θ
        else
            θi      = atan(xi[2], xi[1])
        end

        si = [θ, θdot]
        push!(states, [θ, θdot])
        push!(T, t)
        push!(τ, pen.τ)
    end

    fig.axes[1].plot(getindex.(states, 1), getindex.(states, 2))
    return states, T, τ
end

function test_imitation(pen::dynamics, s)
    θi              = s[1]
    si              = s
    xi              = cartesian(si)
    t               = 0.0f0
    states          = Array{Array{Float32, 1}, 1}()
    T               = Float32[]
    τ               = Float32[]

    for i in 1:200
        imitated_control(pen, si)
        xi, t       = integrate(pen, xi, t)
        θ, θdot     = (atan(xi[2],xi[1]), xi[3])

        if abs(θ-θi)> pi     #this handles wrapping when converting to polar coordinates
            θ_op    = [θ-2.0f0*pi, θ+2.0f0*pi]
            θ       = θ_op[argmin(abs.([θ - 2.0f0*pi - θi, θ + 2.0f0*pi - θi]))]
            θi      = θ
        else
            θi      = atan(xi[2], xi[1])
        end

        si = [θ, θdot]
        push!(states, [θ, θdot])
        push!(T, t)
        push!(τ, pen.τ)
    end

    fig.axes[1].plot(getindex.(states, 1), getindex.(states, 2), 
        linewidth=2, linestyle="-.", label=L"u_{original}(x; p)")
    fig.axes[1].set_title("State evolution", fontsize=16)
    fig.axes[1].set_xlabel(L"$\theta$", fontsize=16)
    fig.axes[1].set_ylabel(L"$\dot{\theta}$", fontsize=16)
    fig.axes[1].tick_params(axis="x", labelsize=16)
    fig.axes[1].tick_params(axis="y", labelsize=16)
    fig.axes[1].legend(fontsize=15)
    
    return states, T, τ
end

function comparison_heat_map()
    data_size   = 100
    θ           = range(-3.14f0, stop = 3.14f0, length = data_size)
    θdot        = range(-5.0f0, stop = 5.0f0, length = data_size)
    v_expert    = zeros(data_size, data_size)
    v_imitated  = zeros(data_size, data_size)

    for i in 1:data_size
        for j in 1:data_size
            s                   = [θ[i], θdot[j]]
            v_expert[i, j]      = V_expert(imi, s)
            v_imitated[i, j]    = imi.V(s)[1]
        end
    end

    c1          = fig.axes[3].pcolormesh(θ, θdot, v_expert, cmap="RdBu", shading="nearest")
    fig.axes[3].set_xlabel("V_expert", fontsize=15) 
    imi.cb1.remove()
    imi.cb1     = plt.colorbar(c1, ax=fig.axes[3])
    c2          = fig.axes[4].pcolormesh(θ, θdot, v_imitated, cmap="RdBu", shading="nearest")
    fig.axes[4].set_xlabel("V_imitated", fontsize=15) 
    imi.cb2.remove()
    imi.cb2     = plt.colorbar(c2, ax=fig.axes[4])

end

function save_weights()
    V = deepcopy(imi.V)
    @save "V_imi.bson" V
end

function load_imitated_weights()
    @load "V_imi.bson" V
    imi.V = deepcopy(V)
end